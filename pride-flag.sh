#!/usr/bin/env bash

#Colors

red="\033[48;5;9m"
RED="\033[48;5;9m"

orange="\033[48;5;208m"
ORANGE="\033[48;5;208m"

yellow="\033[48;5;11m"
YELLOW="\033[48;5;11m"

green="\033[48;5;40m"
GREEN="\033[48;5;40m"

blue="\033[48;5;27m"
BLUE="\033[48;5;27m"

purple="\033[48;5;91m"
PURPLE="\033[48;5;91m"

NC="\033[00m"


echo "$______________________________________"
echo -e "${RED}                            ${NC}"
echo -e "${ORANGE}                            ${NC} "
echo -e "${YELLOW}                            ${NC} "
echo -e "${GREEN}                            ${NC}"
echo -e "${BLUE}                            ${NC}"
echo -e "${PURPLE}                            ${NC}"
echo "$______________________________________"
